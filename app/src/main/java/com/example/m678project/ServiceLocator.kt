package com.example.m678project

import com.example.m678project.dao.CategoryDao
import com.example.m678project.dao.Dao
import com.example.m678project.dao.UserDao
import com.example.m678project.dao.WaypointDao
import com.example.m678project.model.User

object ServiceLocator {
    val dao = Dao()
    lateinit var currentUser: User
    lateinit var userDao: UserDao
    lateinit var waypointDao: WaypointDao
    lateinit var categoryDao: CategoryDao

    fun configureRealm() {
        val realm = dao.mongoRealm.realm
        userDao = UserDao(realm, dao.mongoRealm.realmApp.currentUser!!.id)
        waypointDao = WaypointDao(realm, dao.mongoRealm.realmApp.currentUser!!.id)
        categoryDao = CategoryDao(realm, dao.mongoRealm.realmApp.currentUser!!.id)
    }
}