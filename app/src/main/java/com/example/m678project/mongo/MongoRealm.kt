package com.example.m678project.mongo

import com.example.m678project.ServiceLocator
import com.example.m678project.model.User
import com.example.m678project.model.Waypoint
import com.example.m678project.mongoItem.CategoryItem
import com.example.m678project.mongoItem.UserItem
import com.example.m678project.mongoItem.WaypointItem
import com.google.android.gms.maps.model.LatLng
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.log.LogLevel
import io.realm.kotlin.mongodb.App
import io.realm.kotlin.mongodb.AppConfiguration
import io.realm.kotlin.mongodb.subscriptions
import io.realm.kotlin.mongodb.sync.SyncConfiguration
import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey


class MongoRealm {
    val realmApp = App.create(
        AppConfiguration.Builder("application-0-qvwjt")
            .log(LogLevel.WARN)
            .build())

    lateinit var realm: Realm

    suspend fun configure() {
        val user = realmApp.currentUser!!
        val config = SyncConfiguration.Builder(user, setOf(UserItem::class, WaypointItem::class, CategoryItem::class))
            .initialSubscriptions { realm ->
                add(
                    realm.query<UserItem>(),
                    "All UserItem"
                )
                add(
                    realm.query<WaypointItem>(),
                    "All WaypointItem"
                )
                add(
                    realm.query<CategoryItem>(),
                    "All CategoryItem"
                )
            }
            .waitForInitialRemoteData()
            .build()
        realm = Realm.open(config)
        realm.subscriptions.waitForSynchronization()

        ServiceLocator.configureRealm()
    }
}