package com.example.m678project.mongo

import com.example.m678project.model.Waypoint
import com.example.m678project.mongoItem.WaypointItem
import com.google.android.gms.maps.model.LatLng
import com.mongodb.ConnectionString
import com.mongodb.MongoClientSettings
import com.mongodb.ServerApi
import com.mongodb.ServerApiVersion
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoClients
import com.mongodb.client.MongoCollection
import com.mongodb.client.MongoDatabase
import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.annotations.PrimaryKey
import org.bson.Document
import org.bson.codecs.configuration.CodecRegistries
import org.bson.codecs.pojo.PojoCodecProvider


class Mongo {
    private val pojoCodecProvider = PojoCodecProvider.builder().automatic(true).build()
    private val pojoCodecRegistry = CodecRegistries.fromRegistries(
        MongoClientSettings.getDefaultCodecRegistry(),
        CodecRegistries.fromProviders(pojoCodecProvider)
    )
    private val connectionString =
            ConnectionString("mongodb+srv://franciscomejias:ITB2021318@cluster0.vquglpf.mongodb.net/?retryWrites=true&w=majority")
    private val settings: MongoClientSettings = MongoClientSettings.builder()
        .applyConnectionString(connectionString)
        .serverApi(
            ServerApi.builder()
                .version(ServerApiVersion.V1)
                .build()
        )
        .build()
    private val mongoClient: MongoClient = MongoClients.create(settings)

    private val database: MongoDatabase = mongoClient.getDatabase("todo")
        .withCodecRegistry(pojoCodecRegistry)

    private val collectionDocument = database.getCollection("WaypointItem")

    private fun Document.toWaypoint() = Waypoint(
        id = this.get("id_", ObjectId::class.java),
        icon = ByteArray(0),
        categoryId = this.get("categoryId", ObjectId::class.java),
        title = this.getString("title"),
        position = LatLng(this.getDouble("lat"), this.getDouble("let")),
        description = this.getString("description")
    )

    fun getAll() =
        collectionDocument.find().toList().map { it.toWaypoint() }
}
