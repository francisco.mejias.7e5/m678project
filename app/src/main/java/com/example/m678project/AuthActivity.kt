package com.example.m678project

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.findNavController
import com.example.m678project.databinding.AuthActivityBinding
import com.example.m678project.fragments.LoginFragment


class AuthActivity : AppCompatActivity() {
    private lateinit var binding: AuthActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = AuthActivityBinding.inflate(layoutInflater)

        setContentView(binding.root)
    }
}