package com.example.m678project.model

import com.google.android.gms.maps.model.LatLng
import io.realm.kotlin.types.ObjectId

class Category(
    var _id: ObjectId?,
    var name: String
)