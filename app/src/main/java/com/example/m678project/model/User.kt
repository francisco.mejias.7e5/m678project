package com.example.m678project.model

class User(
    var username: String,
    var name: String,
    var picture: ByteArray,
    var isAdmin: Boolean
)