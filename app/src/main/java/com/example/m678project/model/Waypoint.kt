package com.example.m678project.model

import com.google.android.gms.maps.model.LatLng
import io.realm.kotlin.types.ObjectId

class Waypoint(
    var id: ObjectId?,
    var categoryId: ObjectId,
    var title: String,
    var icon: ByteArray,
    var position: LatLng,
    var description: String
)