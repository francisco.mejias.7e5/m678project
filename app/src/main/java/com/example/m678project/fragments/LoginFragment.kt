package com.example.m678project.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.m678project.MainActivity
import com.example.m678project.R
import com.example.m678project.ServiceLocator
import com.example.m678project.databinding.FragmentLoginBinding
import com.example.m678project.model.User
import kotlinx.coroutines.runBlocking

class LoginFragment : Fragment() {
    private lateinit var binding: FragmentLoginBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)

        binding.loginButton.setOnClickListener {
            runBlocking {
                val username = binding.editTextUsername.text
                val password = binding.editTextPassword.text
                if (username.isEmpty() || password.isEmpty()) {
                    binding.editTextUsername.error = "Can't be empty"
                    binding.editTextPassword.error = "Can't be empty"
                } else {
                    if (ServiceLocator.dao.login(username.toString(), password.toString())) {
                        ServiceLocator.currentUser = ServiceLocator.userDao.get()
                        startActivity(Intent(this@LoginFragment.context, MainActivity::class.java))
                    } else {
                        binding.editTextUsername.error = "Incorrect username or password"
                        binding.editTextPassword.error = "Incorrect username or password"
                    }
                }
            }
        }

        binding.registerRedirection.setOnClickListener {
            findNavController().navigate(R.id.registerFragment)
        }

        return binding.root
    }
}