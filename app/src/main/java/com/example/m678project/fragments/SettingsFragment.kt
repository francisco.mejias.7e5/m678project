package com.example.m678project.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.m678project.AuthActivity
import com.example.m678project.MainActivity
import com.example.m678project.ServiceLocator
import com.example.m678project.databinding.FragmentSettingsBinding
import com.example.m678project.model.Waypoint
import com.google.android.gms.maps.model.MarkerOptions


class SettingsFragment : Fragment() {
    private lateinit var binding: FragmentSettingsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSettingsBinding.inflate(layoutInflater)

        (activity as MainActivity).fab.visibility = View.GONE

        binding.logoutButton.setOnClickListener {
            ServiceLocator.dao.logout()
            startActivity(Intent(this.context, AuthActivity::class.java))
        }

        return binding.root
    }
}