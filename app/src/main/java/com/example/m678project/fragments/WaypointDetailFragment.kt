package com.example.m678project.fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.m678project.R
import com.example.m678project.ServiceLocator
import com.example.m678project.databinding.FragmentWaypointDetailBinding
import io.realm.kotlin.types.ObjectId


class WaypointDetailFragment : Fragment() {
    private lateinit var binding: FragmentWaypointDetailBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWaypointDetailBinding.inflate(layoutInflater)

        val objectId = ObjectId.from(requireArguments().getByteArray("id")!!)
        val waypoint = ServiceLocator.waypointDao.getItemById(objectId)

        binding.iconWaypoint.setImageBitmap(BitmapFactory.decodeByteArray(waypoint.icon, 0, waypoint.icon.size))
        binding.titleWaypoint.text = waypoint.title
        binding.categoryWaypoint.text = ServiceLocator.categoryDao.getItemById(waypoint.categoryId).name
        binding.descriptionWaypoint.text = waypoint.description

        binding.moveTo.setOnClickListener {
            val bundle = Bundle()
            bundle.putDouble("lat", waypoint.position.latitude)
            bundle.putDouble("lng", waypoint.position.longitude)
            findNavController().navigate(R.id.mapsFragment, bundle)
        }

        return binding.root
    }
}