package com.example.m678project.fragments

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.m678project.MainActivity
import com.example.m678project.ServiceLocator
import com.example.m678project.databinding.FragmentUserInfoBinding


class UserInfoFragment : Fragment() {
    private lateinit var binding: FragmentUserInfoBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentUserInfoBinding.inflate(layoutInflater)

        (activity as MainActivity).fab.visibility = View.GONE

        val user = ServiceLocator.currentUser

        binding.name.text = user.name
        binding.username.text = user.username
        binding.image.setImageBitmap(BitmapFactory.decodeByteArray(user.picture, 0, user.picture.size))

        return binding.root
    }
}