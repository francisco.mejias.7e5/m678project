package com.example.m678project.fragments

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.SpinnerAdapter
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.m678project.MainActivity
import com.example.m678project.R
import com.example.m678project.ServiceLocator
import com.example.m678project.databinding.FragmentMapsBinding
import com.example.m678project.model.Category
import com.example.m678project.model.Waypoint
import com.google.android.gms.maps.CameraUpdate
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptor
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.maps.android.ktx.awaitMap
import io.realm.kotlin.types.ObjectId
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.ByteArrayOutputStream
import java.io.File
import java.net.URI


class MapsFragment : Fragment() {
    private lateinit var binding: FragmentMapsBinding

    lateinit var mapFragment: SupportMapFragment
    lateinit var icon: BitmapDescriptor
    lateinit var gMap: GoogleMap

    private val categories = ServiceLocator.categoryDao.listFlow()

    @SuppressLint("UseCompatLoadingForDrawables", "ResourceType")
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMapsBinding.inflate(layoutInflater)

        (activity as MainActivity).fab.visibility = View.VISIBLE
        (activity as MainActivity).bottomBar.visibility = View.VISIBLE

        mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(requireActivity() as MainActivity)

        val waypoint = resources.getDrawable(R.drawable.logo_waipoint_png, null).toBitmap()
        val smallWaypoint = Bitmap.createScaledBitmap(waypoint, 100, 100, false)

        icon = BitmapDescriptorFactory.fromBitmap(smallWaypoint)

        MainScope().launch {
            onMapReady()
        }

        binding.includeRegisterWaypoint.addButton.setOnClickListener {
            val markerOptions = MarkerOptions()
                .icon(icon)
                .title(binding.includeRegisterWaypoint.editTextTitle.text.toString())
                .position(gMap.cameraPosition.target)

            gMap.addMarker(markerOptions)

            val thumbImage = ThumbnailUtils.extractThumbnail(
                binding.includeRegisterWaypoint.waypointImage.drawable.toBitmap(),
                200,
                200
            )
            val stream = ByteArrayOutputStream()
            thumbImage.compress(Bitmap.CompressFormat.PNG, 100, stream)
            val thumbByteArray: ByteArray = stream.toByteArray()
            thumbImage.recycle()

            runBlocking {
                ServiceLocator.waypointDao.insert(Waypoint(
                    null,
                    categories.first()[binding.includeRegisterWaypoint.spinnerCategories.selectedItemPosition]._id!!,
                    markerOptions.title!!,
                    thumbByteArray,
                    markerOptions.position,
                    binding.includeRegisterWaypoint.editTextDescription.text.toString()
                ))
            }
            binding.includeRegisterWaypoint.root.visibility = View.GONE
        }

        runBlocking {
            val arrayAdapter = ArrayAdapter(this@MapsFragment.requireContext(), android.R.layout.simple_spinner_item, categories.first().map { it.name })
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

            with(binding.includeRegisterWaypoint.spinnerCategories) {
                adapter = arrayAdapter
                setSelection(0, false)
                prompt = "Select category"
                gravity = Gravity.CENTER
            }
        }

        binding.includeRegisterWaypoint.addCategoryButton.setOnClickListener {
            binding.includeRegisterWaypoint.root.visibility = View.GONE
            binding.includeRegisterCategory.root.visibility = View.VISIBLE
        }

        binding.includeRegisterCategory.addButton.setOnClickListener {
            ServiceLocator.categoryDao.insert(Category(
                null,
                binding.includeRegisterCategory.editTextName.text.toString()
            ))
            fillSpinner()

            binding.includeRegisterCategory.root.visibility = View.GONE
            binding.includeRegisterWaypoint.root.visibility = View.VISIBLE
        }

        binding.includeRegisterWaypoint.waypointImage.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("title", binding.includeRegisterWaypoint.editTextTitle.text.toString())
            bundle.putString("description", binding.includeRegisterWaypoint.editTextDescription.text.toString())
            bundle.putInt("spinner_position", binding.includeRegisterWaypoint.spinnerCategories.selectedItemPosition)
            bundle.putDouble("lat", gMap.cameraPosition.target.latitude)
            bundle.putDouble("lng", gMap.cameraPosition.target.longitude)

            findNavController().navigate(R.id.cameraFragment, bundle)
        }

        return binding.root
    }

    private fun fillSpinner() {
        runBlocking {

            (binding.includeRegisterWaypoint.spinnerCategories.adapter as ArrayAdapter<String>).clear()
            (binding.includeRegisterWaypoint.spinnerCategories.adapter as ArrayAdapter<String>).addAll(categories.first().map { it.name })
        }
    }

    suspend fun onMapReady() {
        gMap = mapFragment.awaitMap()

        val waypointFlow = ServiceLocator.waypointDao.listFlow()

        waypointFlow.first().forEach {
            val markerOptions = MarkerOptions()
                .icon(icon)
                .title(it.title)
                .position(it.position)

            gMap.addMarker(markerOptions)
        }

        (activity as MainActivity).fab.setOnClickListener {
            binding.includeRegisterWaypoint.root.visibility = View.VISIBLE
        }

        if (arguments != null && arguments?.isEmpty == false) {
            val args = requireArguments()
            if (args.get("image_file") == null) {
                val lat = args.getDouble("lat")
                val lng = args.getDouble("lng")
                gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), gMap.cameraPosition.zoom), 1, null)
            } else {
                val imageFile = args.getByteArray("image_file")
                val title = args.getString("title")!!
                val description = args.getString("description")!!
                val spinnerPosition = args.getInt("spinner_position")
                val lat = args.getDouble("lat")
                val lng = args.getDouble("lng")

                binding.includeRegisterWaypoint.root.visibility = View.VISIBLE

                val bitmap = BitmapFactory.decodeByteArray(imageFile, 0, imageFile!!.size)

                binding.includeRegisterWaypoint.waypointImage.setImageBitmap(bitmap)
                binding.includeRegisterWaypoint.editTextTitle.setText(title)
                binding.includeRegisterWaypoint.editTextDescription.setText(description)
                binding.includeRegisterWaypoint.spinnerCategories.setSelection(spinnerPosition)

                gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(lat, lng), gMap.cameraPosition.zoom), 1, null)
            }
        }
    }
}