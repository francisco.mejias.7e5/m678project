package com.example.m678project.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.m678project.MainActivity
import com.example.m678project.R
import com.example.m678project.ServiceLocator
import com.example.m678project.databinding.FragmentRegisterBinding
import com.example.m678project.model.User
import kotlinx.coroutines.runBlocking

class RegisterFragment : Fragment() {
    private lateinit var binding: FragmentRegisterBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(layoutInflater)

        binding.registerButton.setOnClickListener {
            runBlocking {
                val username = binding.editTextUsername.text
                val name = binding.editTextName.text
                val password = binding.editTextPassword.text
                if (username.isEmpty() || name.isEmpty() || password.isEmpty()) {
                    binding.editTextUsername.error = "Can't be empty"
                    binding.editTextPassword.error = "Can't be empty"
                    binding.editTextName.error = "Can't be empty"
                } else {
                    if (password.length < 6 || password.length > 128) {
                        binding.editTextPassword.error = "Password must be between 6 and 128 characters"
                    } else {
                        if (ServiceLocator.dao.register(username.toString(), password.toString())) {
                            ServiceLocator.userDao.insert(User(
                                username.toString(),
                                name.toString(),
                                ByteArray(0),
                                false
                            ))
                            ServiceLocator.currentUser = ServiceLocator.userDao.get()
                            startActivity(Intent(this@RegisterFragment.context, MainActivity::class.java))
                        } else {
                            binding.editTextUsername.error = "This username is already taken"
                        }
                    }
                }
            }
        }

        binding.loginRedirection.setOnClickListener {
            findNavController().navigate(R.id.loginFragment)
        }

        return binding.root
    }
}