package com.example.m678project.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TableRow.LayoutParams
import androidx.core.view.marginBottom
import androidx.core.view.updateLayoutParams
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.m678project.MainActivity
import com.example.m678project.ServiceLocator
import com.example.m678project.adapter.WaypointRecyclerViewAdapter
import com.example.m678project.databinding.FragmentWaypointListBinding
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking


class WaypointListFragment : Fragment() {
    private lateinit var binding: FragmentWaypointListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWaypointListBinding.inflate(layoutInflater)

        val mainActivity =  (activity as MainActivity)
        mainActivity.fab.visibility = View.GONE

        runBlocking {
            with(binding.root) {
                layoutManager = LinearLayoutManager(context)
                adapter = WaypointRecyclerViewAdapter(ServiceLocator.waypointDao.listFlow().first().toMutableList().asReversed())
            }
        }

        val layoutParams = LayoutParams()
        layoutParams.bottomMargin = mainActivity.bottomBar.height

        binding.root.layoutParams = layoutParams

        return binding.root
    }
}