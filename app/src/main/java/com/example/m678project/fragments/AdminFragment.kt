package com.example.m678project.fragments

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.m678project.AuthActivity
import com.example.m678project.ServiceLocator
import com.example.m678project.adapter.WaypointAdminRecyclerViewAdapter
import com.example.m678project.databinding.FragmentAdminBinding
import com.example.m678project.model.Waypoint
import com.example.m678project.mongo.Mongo
import kotlinx.coroutines.flow.first
import java.util.Collections.emptyList


class AdminFragment : Fragment() {
    private lateinit var binding: FragmentAdminBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentAdminBinding.inflate(layoutInflater)

        with(binding.userRecyclerView) {
            layoutManager = LinearLayoutManager(context)
//            adapter = WaypointAdminRecyclerViewAdapter(Mongo().getAll().toMutableList())
            adapter = WaypointAdminRecyclerViewAdapter(emptyList<Waypoint>().toMutableList())
        }

        binding.logout.setOnClickListener {
            ServiceLocator.dao.logout()
            startActivity(Intent(this.context, AuthActivity::class.java))
        }

        return binding.root
    }
}