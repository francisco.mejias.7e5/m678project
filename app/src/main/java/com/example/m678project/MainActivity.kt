package com.example.m678project

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.findFragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.example.m678project.databinding.MainActivityBinding
import com.example.m678project.fragments.AdminFragment
import com.example.m678project.fragments.MapsFragment
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.LatLng
import com.google.android.material.bottomappbar.BottomAppBar
import com.google.android.material.floatingactionbutton.FloatingActionButton
import kotlinx.coroutines.*
import kotlin.properties.Delegates


class MainActivity : AppCompatActivity(), OnMapReadyCallback {
    private lateinit var binding: MainActivityBinding

    private lateinit var mMap: GoogleMap
    private var locationPermissionGranted = false
    private var lastKnownLocation: Location? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val defaultLocation = LatLng(-33.8523341, 151.2106085)
    lateinit var fab: FloatingActionButton
    lateinit var bottomBar: BottomAppBar

    @SuppressLint("ResourceType")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        fab = binding.fab

        if (ServiceLocator.currentUser.isAdmin) {
            runAdmin()
        } else {
            runNotAdmin()
        }

        setContentView(binding.root)
    }

    private fun runAdmin() {
        supportFragmentManager.beginTransaction().replace(R.id.fragmentContainerView,
            AdminFragment()).commit()

        binding.root.removeView(binding.bottomNavigationView)
        binding.root.removeView(binding.fab)
        binding.root.removeView(binding.bottomAppBar)
    }

    private fun runNotAdmin() {
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        binding.bottomNavigationView.background = null
        binding.bottomNavigationView.menu.getItem(2).isEnabled = false

        bottomBar = binding.bottomAppBar

        binding.bottomNavigationView.setOnItemSelectedListener {
            when(it.title) {
                resources.getString(R.string.home) -> {
                    if (binding.fragmentContainerView.findNavController().currentDestination?.id != R.id.mapsFragment) {
                        binding.fragmentContainerView.findNavController().navigate(R.id.mapsFragment)
                    }
                }
                resources.getString(R.string.search) -> {
                    if (binding.fragmentContainerView.findNavController().currentDestination?.id != R.id.waypointListFragment) {
                        binding.fragmentContainerView.findNavController().navigate(R.id.waypointListFragment)
                    }
                }
                resources.getString(R.string.profile) -> {
                    if (binding.fragmentContainerView.findNavController().currentDestination?.id != R.id.userInfoFragment) {
                        binding.fragmentContainerView.findNavController().navigate(R.id.userInfoFragment)
                    }
                }
                resources.getString(R.string.settings) -> {
                    if (binding.fragmentContainerView.findNavController().currentDestination?.id != R.id.settingsFragment) {
                        binding.fragmentContainerView.findNavController().navigate(R.id.settingsFragment)
                    }
                }
            }
            true
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        updateLocationUI()
        getDeviceLocation()


        CoroutineScope(Dispatchers.Main).launch {
            for (i in 0..6) {
                delay(500)
                if (ActivityCompat.checkSelfPermission(
                        this@MainActivity,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED &&
                    ActivityCompat.checkSelfPermission(
                        this@MainActivity,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) == PackageManager.PERMISSION_GRANTED
                ) {
                    mMap.isMyLocationEnabled = true
                    mMap.uiSettings.isMyLocationButtonEnabled = true
                    break
                }
            }
        }

        mMap.uiSettings.isCompassEnabled = true
        mMap.uiSettings.isRotateGesturesEnabled = true
        mMap.uiSettings.isZoomGesturesEnabled = true

        mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
    }

    private fun updateLocationUI() {
        try {
            if (locationPermissionGranted) {
                mMap.isMyLocationEnabled = true
                mMap.uiSettings.isMyLocationButtonEnabled = true
            } else {
                mMap.isMyLocationEnabled = false
                mMap.uiSettings.isMyLocationButtonEnabled = false
                lastKnownLocation = null
                getLocationPermission()
            }
        } catch (e: SecurityException) {
            println("Exception: %s" + e.message + e)
        }
    }

    private fun getLocationPermission() {
        if (ContextCompat.checkSelfPermission(this.applicationContext,
                Manifest.permission.ACCESS_FINE_LOCATION)
            == PackageManager.PERMISSION_GRANTED) {
            locationPermissionGranted = true
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),1)
        }
    }

    private fun getDeviceLocation() {
        try {
            if (locationPermissionGranted) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                LatLng(lastKnownLocation!!.latitude,
                                    lastKnownLocation!!.longitude), 15F))
                        }
                    } else {
                        println("Current location is null. Using defaults.")
                        println("Exception: %s" + task.exception)
                        mMap.moveCamera(CameraUpdateFactory
                            .newLatLngZoom(defaultLocation, 15F))
                        mMap.uiSettings.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            println("Exception: %s" + e.message + e)
        }
    }
    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        locationPermissionGranted = false
        when (requestCode) {
            1 -> {
                if (grantResults.isNotEmpty() &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    locationPermissionGranted = true
                }
            }
            else -> super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        }
        updateLocationUI()
    }
}
