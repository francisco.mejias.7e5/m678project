package com.example.m678project.adapter

import android.graphics.BitmapFactory
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.example.m678project.databinding.FragmentWaypointAdminBinding
import com.example.m678project.model.User
import com.example.m678project.model.Waypoint

class WaypointAdminRecyclerViewAdapter(
    private val values: MutableList<Waypoint>
) : RecyclerView.Adapter<WaypointAdminRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentWaypointAdminBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        val bitmap = BitmapFactory.decodeByteArray(item.icon, 0, item.icon.size)
        holder.iconWaypoint.setImageBitmap(bitmap)
        holder.titleWaypoint.text = item.title

        holder.button.setOnClickListener {

        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentWaypointAdminBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val iconWaypoint: ImageView = binding.iconWaypoint
        val titleWaypoint: TextView = binding.titleWaypoint
        val button: Button = binding.button
    }
}
