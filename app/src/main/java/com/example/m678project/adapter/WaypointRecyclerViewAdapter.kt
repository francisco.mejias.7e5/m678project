package com.example.m678project.adapter

import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.m678project.R
import com.example.m678project.databinding.FragmentWaypointBinding
import com.example.m678project.model.Waypoint
import io.realm.kotlin.ext.asBsonObjectId
import io.realm.kotlin.types.ObjectId

class WaypointRecyclerViewAdapter(
    private val values: MutableList<Waypoint>
) : RecyclerView.Adapter<WaypointRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            FragmentWaypointBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = values[position]
        val bitmap = BitmapFactory.decodeByteArray(item.icon, 0, item.icon.size)
        holder.iconWaypoint.setImageBitmap(bitmap)
        holder.titleWaypoint.text = item.title

        setOnClickToItem((holder.itemView as LinearLayout), item.id!!)
    }

    private fun setOnClickToItem(itemLayout: LinearLayout, id: ObjectId) {
        itemLayout.setOnClickListener {
            val bundle = Bundle()
            bundle.putByteArray("id", id.asBsonObjectId().toByteArray())
            itemLayout.findNavController().navigate(R.id.action_waypointListFragment_to_waypointDetailFragment, bundle)
        }
    }

    override fun getItemCount(): Int = values.size

    inner class ViewHolder(binding: FragmentWaypointBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val iconWaypoint: ImageView = binding.iconWaypoint
        val titleWaypoint: TextView = binding.titleWaypoint
    }
}
