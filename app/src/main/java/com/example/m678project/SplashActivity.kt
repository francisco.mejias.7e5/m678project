package com.example.m678project

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.m678project.dao.UserDao
import com.example.m678project.dao.WaypointDao
import com.example.m678project.databinding.SplashScreenBinding
import io.realm.kotlin.mongodb.syncSession
import kotlinx.coroutines.runBlocking
import kotlin.time.Duration

@SuppressLint("CustomSplashScreen")
open class SplashActivity: AppCompatActivity() {
    private lateinit var binding: SplashScreenBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = SplashScreenBinding.inflate(layoutInflater)

        val intent = Intent(this,
            if (ServiceLocator.dao.loggedIn) {
                runBlocking {
                    ServiceLocator.dao.mongoRealm.configure()
                }
                ServiceLocator.currentUser = ServiceLocator.userDao.get()
                MainActivity::class.java
            } else {
                AuthActivity::class.java
            }
        )

        startActivity(intent)
    }
}