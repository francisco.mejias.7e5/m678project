package com.example.m678project.mongoItem

import com.example.m678project.model.User
import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

open class UserItem(
    @PrimaryKey
    var _id: ObjectId = ObjectId.create(),
    var username: String? = null,
    var name: String? = null,
    var picture: ByteArray? = null,
    var isAdmin: Boolean? = null,
    var owner_id: String = ""
) : RealmObject {
    fun toUser() = User(username ?: "", name ?: "", picture ?: ByteArray(0), isAdmin ?: false)

    constructor() : this(owner_id = "")
    constructor(ownerId: String, user: User): this(
        username = user.username,
        name = user.name,
        picture = user.picture,
        isAdmin = user.isAdmin,
        owner_id = ownerId
    )
}