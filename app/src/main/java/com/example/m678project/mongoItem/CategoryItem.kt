package com.example.m678project.mongoItem

import com.example.m678project.model.Category
import com.example.m678project.model.Waypoint
import com.google.android.gms.maps.model.LatLng
import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

open class CategoryItem(
    @PrimaryKey
    var _id: ObjectId = ObjectId.create(),
    var name: String? = null,
    var owner_id: String = ""
) : RealmObject {
    fun toCategory() = Category(_id, name ?: "")

    constructor() : this(owner_id = "")
    constructor(ownerId: String, category: Category): this(
        name = category.name,
        owner_id = ownerId
    )
}