package com.example.m678project.mongoItem

import com.example.m678project.model.Waypoint
import com.google.android.gms.maps.model.LatLng
import io.realm.kotlin.types.ObjectId
import io.realm.kotlin.types.RealmObject
import io.realm.kotlin.types.annotations.PrimaryKey

open class WaypointItem(
    @PrimaryKey
    var _id: ObjectId = ObjectId.create(),
    var categoryId: ObjectId? = null,
    var title: String? = null,
    var icon: ByteArray? = null,
    var lat: Double? = null,
    var lng: Double? = null,
    var description: String? = null,
    var owner_id: String = ""
) : RealmObject {
    fun toWaypoint() = Waypoint(
        _id,
        categoryId ?: ObjectId.create(),
        title ?: "",
        icon ?: ByteArray(0),
        LatLng(lat ?: 0.0, lng ?: 0.0),
        description ?: ""
    )

    constructor() : this(owner_id = "")
    constructor(ownerId: String, waypoint: Waypoint): this(
        title = waypoint.title,
        categoryId = waypoint.categoryId,
        icon = waypoint.icon,
        lat = waypoint.position.latitude,
        lng = waypoint.position.longitude,
        description = waypoint.description,
        owner_id = ownerId
    )
}