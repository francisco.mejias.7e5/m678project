package com.example.m678project.dao

import com.example.m678project.model.Waypoint
import com.example.m678project.mongoItem.WaypointItem
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.types.ObjectId
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class WaypointDao(
    private val realm: Realm,
    private val userId: String
) {
    fun listFlow() : Flow<List<Waypoint>> = realm.query<WaypointItem>().find().asFlow().map { it.list.map { it.toWaypoint() } }

    fun getItemById(id: ObjectId) : Waypoint = realm.query<WaypointItem>("_id = $0", id).find().first().toWaypoint()

    fun insert(waypoint: Waypoint) {
        realm.writeBlocking {
            val item = WaypointItem(userId, waypoint)
            copyToRealm(item)
        }
    }
}