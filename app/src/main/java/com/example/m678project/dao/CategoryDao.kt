package com.example.m678project.dao

import com.example.m678project.model.Category
import com.example.m678project.model.Waypoint
import com.example.m678project.mongoItem.CategoryItem
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query
import io.realm.kotlin.types.ObjectId
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class CategoryDao(
    private val realm: Realm,
    private val userId: String
) {
    fun listFlow() : Flow<List<Category>> = realm.query<CategoryItem>().find().asFlow().map { it.list.map { it.toCategory() } }

    fun getItemById(id: ObjectId) : Category = realm.query<CategoryItem>("_id = $0", id).find().first().toCategory()

    fun insert(category: Category) {
        realm.writeBlocking {
            val item = CategoryItem(userId, category)
            copyToRealm(item)
        }
    }
}