package com.example.m678project.dao

import com.example.m678project.mongo.MongoRealm
import io.realm.kotlin.mongodb.Credentials
import io.realm.kotlin.mongodb.exceptions.InvalidCredentialsException
import io.realm.kotlin.mongodb.exceptions.UserAlreadyExistsException
import kotlinx.coroutines.runBlocking

open class Dao {
    val mongoRealm = MongoRealm()

    val loggedIn get() =  mongoRealm.realmApp.currentUser?.loggedIn ?: false

    fun logout() {
        runBlocking {
            mongoRealm.realmApp.currentUser!!.logOut()
        }
    }

    suspend fun register(username: String, password: String): Boolean {
        try {
            mongoRealm.realmApp.emailPasswordAuth.registerUser(username, password)
        } catch (e: UserAlreadyExistsException) {
            return false
        }
        login(username, password)
        return true
    }

    suspend fun login(username: String, password: String): Boolean {
        val credentials = Credentials.emailPassword(username, password)
        try {
            mongoRealm.realmApp.login(credentials)
        } catch (e: InvalidCredentialsException) {
            return false
        }
        mongoRealm.configure()
        return true
    }
}