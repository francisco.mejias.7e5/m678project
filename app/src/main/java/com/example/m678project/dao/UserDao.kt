package com.example.m678project.dao

import com.example.m678project.model.User
import com.example.m678project.mongoItem.UserItem
import io.realm.kotlin.Realm
import io.realm.kotlin.ext.query

class UserDao(
    private val realm: Realm,
    private val userId: String
) {
    fun getAll(): List<User> =
        realm.query<UserItem>().find().map { it.toUser() }

    fun insert(user: User) {
        realm.writeBlocking {
            val item = UserItem(userId, user)
            copyToRealm(item)
        }
    }

    fun get(): User =
        realm.query<UserItem>().find().first().toUser()
}